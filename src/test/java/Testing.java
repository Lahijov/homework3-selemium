import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Testing {

    @Test
    void trash1() {

        WebDriver webDriver = new ChromeDriver();
        Actions action = new Actions(webDriver);
        webDriver.manage().window().maximize();

        try {
            webDriver.get("https://www.globalsqa.com/demo-site/draganddrop/");

            WebElement iframe = webDriver.findElement(By.xpath("//*[@id=\"post-2669\"]/div[2]/div/div/div[1]/p/iframe"));

            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            webDriver.switchTo().frame(iframe);
            WebElement image = webDriver.findElement(By.xpath("//*[@id=\"gallery\"]/li[1]"));
            WebElement trash = webDriver.findElement(By.xpath("//*[@id=\"trash\"]"));
            action.dragAndDrop(image, trash).perform();
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            List<WebElement> trashSection = webDriver.findElements(By.xpath("//*[@id=\"trash\"]/ul"));
            Assertions.assertTrue(trashSection.size() == 1);

        } finally {
            webDriver.close();
        }
    }

    @Test
    void number2() {

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        try {
            webDriver.get("https://www.globalsqa.com/demo-site/select-dropdown-menu/");

            WebElement menu = webDriver.findElement(By.xpath("//*[@id=\"post-2646\"]/div[2]/div/div/div/p/select"));
            Select dropdown = new Select(menu);
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            System.out.println(dropdown.getOptions().size());
            Assertions.assertTrue(dropdown.getOptions().size() == 249);


        } finally {
            webDriver.close();
        }
    }

    @Test
    void number3() {

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        String name_expected = "Name: Rafig";
        String email_expected = "Email: l.rafik@mail.ru";
        String website_expected = "Website: https://www.youtube.com";
        String experience_expected = "Experience (In Years): 3-5";
        String testing_expected = "Expertise :: Functional Testing";
        String education_expected = "Education: Post Graduate";
        String comment_expected = "Comment: Rafig Lahijov";
        try {
            webDriver.get("https://www.globalsqa.com/samplepagetest/");
            WebElement file = webDriver.findElement(By.xpath("//*[@id=\"wpcf7-f2598-p2599-o1\"]/form/p/span/input"));
            File file1 = new File("D:\\Users\\User\\Desktop\\erorr matlab.PNG");
            file.sendKeys(file1.getCanonicalPath());


            WebElement name = webDriver.findElement(By.xpath("//*[@id=\"g2599-name\"]"));
            name.sendKeys("Rafig");
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement email = webDriver.findElement(By.xpath("//*[@id=\"g2599-email\"]"));
            email.sendKeys("l.rafik@mail.ru");
            WebElement website = webDriver.findElement(By.xpath("//*[@id=\"g2599-website\"]"));
            website.sendKeys("https://www.youtube.com");
            WebElement dropdown = webDriver.findElement(By.xpath("//*[@id=\"g2599-experienceinyears\"]"));
            Select experience = new Select(dropdown);
            experience.selectByIndex(2);
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            WebElement functionalTesting = webDriver.findElement(By.xpath("//*[@id=\"contact-form-2599\"]/form/div[5]/label[2]/input"));
            functionalTesting.click();
            WebElement postGraduate = webDriver.findElement(By.xpath("//*[@id=\"contact-form-2599\"]/form/div[6]/label[3]/input"));
            postGraduate.click();
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            WebElement commentBox = webDriver.findElement(By.xpath("//textarea[@class=\"textarea\"]"));
            commentBox.sendKeys("Rafig Lahijov");

            WebElement submitButton = webDriver.findElement(By.xpath("//button[@type=\"submit\"]"));
            submitButton.click();
            List<WebElement> list = webDriver.findElements(By.xpath("//*[@id=\"contact-form-2599\"]/blockquote/p"));
            Assertions.assertEquals(list.get(0).getText(), name_expected);
            Assertions.assertEquals(list.get(1).getText(), email_expected);
            Assertions.assertEquals(list.get(2).getText(), website_expected);
            Assertions.assertEquals(list.get(3).getText(), experience_expected);
            Assertions.assertEquals(list.get(4).getText(), testing_expected);
            Assertions.assertEquals(list.get(5).getText(), education_expected);
            Assertions.assertEquals(list.get(6).getText(), comment_expected);


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            webDriver.close();
        }
    }

}
